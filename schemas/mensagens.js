Mensagem = new Meteor.Collection('mensagem');
 var Schema = {};

 Schema.Mensagem = new SimpleSchema({
 	nome: {
 		type: String,
 		optional: false
 	},

 conteudo: {
 	type: String,
 	optional: false
 },

 	data: {
		type: Date,
    autoValue: function() {
      if (this.isInsert) {
        return new Date;
      } else if (this.isUpsert) {
        return {$setOnInsert: new Date};
      } else {
        this.unset();
      }
    }
 	},

 	retweets: {
 		type: [Object],
 		optional: true
 	},
 	"retweets.$.nome": {
 		type: String
 	},
 	"retweets.$.conteudo": {
 		type: String
 	},
  "retweets.$.data": {
    type: Date,
    autoValue: function() {
      return new Date();
    }
  }
});

Mensagem.attachSchema(Schema.Mensagem);