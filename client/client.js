Template.formulario.events({
    'submit #tweet': function(event, template) {
      console.log(Session.get());
      var tweet = {};
      tweet.nome = template.find('#nome').value;
      tweet.conteudo = template.find('#conteudo').value;
      if (Session.equals("actual_tweet", null)) {
        Mensagem.insert(tweet);
      }else {
        Mensagem.update({_id: Session.get('actual_tweet')}, {$push: {retweets: tweet}});
      }
      event.preventDefault();
      //reload();
    }
  });

Template.mensagens.timeline = function() {
  return Mensagem.find({}, { sort: { time: -1 }});
};

Template.postsShow.tweet = function() {
  return Mensagem.findOne({_id: Session.get('actual_tweet')});
};