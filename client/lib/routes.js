Router.configure({
  layoutTemplate: 'MasterLayout',
  yieldTemplates: {
    'Header': { to: 'Header' }
    //'Sidebar': { to: 'Sidebar' },
    //'Footer': { to: 'Footer' }
  }

});

Router.map(function() {
  this.route('home', {
  	path: '/',
  	template: 'Messages',
  	onAfterAction: function() {
	  	Session.set('actual_tweet', null);
	  	return Mensagem.find({});
  	}
  });


	this.route('mensagensShow', { 
	  path: '/mensagens/:_id',
	  template: 'Responses',
	  data: function() { 
	  	Session.set('actual_tweet', this.params._id);
	  	return Mensagem.findOne(this.params._id);
	  },
	});
});