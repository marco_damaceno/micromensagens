/*****************************************************************************/
/* Messages: Event Handlers and Helpers */
/*****************************************************************************/
Template.Messages.events({
  /*
   * Example: 
   *  'click .selector': function (e, tmpl) {
   *
   *  }
   */
});

Template.Messages.helpers({
  /*
   * Example: 
   *  items: function () {
   *    return Items.find();
   *  }
   */
});

/*****************************************************************************/
/* Messages: Lifecycle Hooks */
/*****************************************************************************/
Template.Messages.created = function () {
};

Template.Messages.rendered = function () {
};

Template.Messages.destroyed = function () {
};

Template.Messages.timeline = function() {
  return Mensagem.find({}, { sort: { time: -1 }});
};