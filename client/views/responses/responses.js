/*****************************************************************************/
/* Responses: Event Handlers and Helpers */
/*****************************************************************************/
Template.Responses.events({
  /*
   * Example: 
   *  'click .selector': function (e, tmpl) {
   *
   *  }
   */
});

Template.Responses.helpers({
  /*
   * Example: 
   *  items: function () {
   *    return Items.find();
   *  }
   */
});

/*****************************************************************************/
/* Responses: Lifecycle Hooks */
/*****************************************************************************/
Template.Responses.created = function () {
};

Template.Responses.rendered = function () {
};

Template.Responses.destroyed = function () {
};

Template.Responses.tweet = function() {
  return Mensagem.findOne({_id: Session.get('actual_tweet')});
};