/*****************************************************************************/
/* FormMessage: Event Handlers and Helpers */
/*****************************************************************************/
Template.FormMessage.events({
    'submit #tweet': function(event, template) {
        if (Meteor.user()) {
            var tweet = {};
            tweet.nome = Meteor.user().profile.name;
            tweet.conteudo = template.find('#conteudo').value;
            if (tweet.conteudo == "") {
                alert("Favor preencher todos os campos.");
            } else {
                if (Session.equals("actual_tweet", null)) {
                    Mensagem.insert(tweet);
                } else {
                    Mensagem.update({
                        _id: Session.get('actual_tweet')
                    }, {
                        $push: {
                            retweets: tweet
                        }
                    });
                }
                event.preventDefault();
            };
        } else {
            alert("É preciso fazer login primeiro");
            event.preventDefault();
        }
    }
});
Template.FormMessage.helpers({
    /*
     * Example:
     *  items: function () {
     *    return Items.find();
     *  }
     */
});
/*****************************************************************************/
/* FormMessage: Lifecycle Hooks */
/*****************************************************************************/
Template.FormMessage.created = function() {};
Template.FormMessage.rendered = function() {};
Template.FormMessage.destroyed = function() {};