/*****************************************************************************/
/* Accounts: Event Handlers and Helpers */
/*****************************************************************************/
Template.Accounts.events({
    /*
     * Example:
     *  'click .selector': function (e, tmpl) {
     *
     *  }
     */
});
Template.Accounts.helpers({
    /*
     * Example:
     *  items: function () {
     *    return Items.find();
     *  }
     */
});
/*****************************************************************************/
/* Accounts: Lifecycle Hooks */
/*****************************************************************************/
Template.Accounts.created = function() {};
Template.Accounts.rendered = function() {};
Template.Accounts.destroyed = function() {};
Template._loginButtonsLoggedInDropdown.events({
    'click #login-buttons-edit-profile': function(event) {
        event.stopPropagation();
        Template._loginButtons.toggleDropdown();
        Router.go('profileEdit');
    }
});